package com.mygdx.game.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class Hangar extends Activity {

    int flux;
    int booster = 1;
    int weapon = 1;
    TextView fluxDisplay;
    TextView boosterDisplay;
    TextView weaponDisplay;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hangar);
        
        SharedPreferences savFile = this.getSharedPreferences("gamePref", Context.MODE_PRIVATE);
        flux = savFile.getInt("flux", 0);


        fluxDisplay = (TextView) findViewById(R.id.amount);
        fluxDisplay.setText(String.valueOf(flux));
        weaponDisplay = (TextView) findViewById(R.id.weapon);
        weaponDisplay.setText(String.valueOf(weapon));

        boosterDisplay = (TextView) findViewById(R.id.booster);
        boosterDisplay.setText(String.valueOf(booster));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_hangar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private int toggle = 0;

    public void flip(View view) {
        ImageView btn = (ImageView) findViewById(R.id.shipButton);
        if(toggle == 0){
            btn.setImageResource(R.drawable.ship1_red);
            toggle++;
        } else {
            btn.setImageResource(R.drawable.ship1);
            toggle--;
        }

    }

    public void play(View view) {
        startActivity(new Intent(getApplicationContext(), AndroidLauncher.class));
    }

    public void buyFlux(View view){
        flux = flux + 1;
        fluxDisplay.setText(String.valueOf(flux));
    }
    public void buyWeapon(View view){
        if(flux >= 200) {
            flux = flux -200;
            weapon = weapon + 1;
        }
        weaponDisplay.setText(String.valueOf(weapon));
        fluxDisplay.setText(String.valueOf(flux));
    }

    public void buyBooster(View view){
        if(flux >= 200) {
            flux = flux -200;
            booster = booster + 1;
        }
        boosterDisplay.setText(String.valueOf(booster));
        fluxDisplay.setText(String.valueOf(flux));
    }

}
