package com.mygdx.game.android;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.mygdx.game.FluxXor;

public class AndroidLauncher extends AndroidApplication implements FluxXor.MyGameCallback{
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		SharedPreferences savFile = this.getSharedPreferences("gamePrefs",Context.MODE_PRIVATE);

        int flux = savFile.getInt("flux", 0);
		FluxXor myGame = new FluxXor(flux);
		myGame.setMyGameCallback(this);
		
		initialize(myGame, config);
	}
	
	
	@Override
	public void onActivityA(int newFlux){
		SharedPreferences savFile = this.getSharedPreferences("gamePref",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = savFile.edit();
        editor.putInt("flux", newFlux);
        editor.commit();
        
		Intent intent = new Intent(this, FluXor.class);
		startActivity(intent);
	}
}
