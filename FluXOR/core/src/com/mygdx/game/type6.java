package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class type6 extends Mob
	{

	int xpos;
	int ypos;
	
	public type6(boolean red, boolean left,int startingx,int startingy,float playerx,float playery)
	{
		xpos = startingx;
		ypos = startingy;
		super.type6move = false;
		super.firable = false;
		super.isred = red;
		super.isLeft=left;
		super.x = startingx;
		super.y =startingy;
		super.HP = 15;
		super.width = 200;
		super.height = 150;
		if(left)
		{
			if(red)
			{
			super.sprite = new Texture(Gdx.files.internal("type6 red.png"));
			}
			else
			{
			super.sprite = new Texture(Gdx.files.internal("type6 blue.png"));	
			}
		}
		else
		{
			if(red)
			{
			super.sprite = new Texture(Gdx.files.internal("type6 red.png"));
			}
			else
			{
			super.sprite = new Texture(Gdx.files.internal("type6 blue.png"));	
			}
		}
	}
	
	
	
	public void update()
	{
		if(isLeft)
		{
			super.x = 0;
			if(super.y > 650 && !type6move )
			{
			super.y -= 300*Gdx.graphics.getDeltaTime();
			}
			else
			{
				super.firable = true;
			}
			if(type6move )
			{
			super.y += 300*Gdx.graphics.getDeltaTime();
			}
		}
		else
		{
			super.x = 280;
			if(super.y > 650 && !type6move )
			{
			super.y -= 300*Gdx.graphics.getDeltaTime();
			}
			else
			{
				super.firable = true;
			}
			if(type6move )
			{
			super.y += 300*Gdx.graphics.getDeltaTime();
			}
		}
	}
	
	public Array<EBullets> createbull(float pxpos, float pypos)
	{
		
		Array<EBullets> eb = new Array<EBullets>();
		
		Vector2 vector = new Vector2((pxpos-(super.x+(super.width/2))),(pypos-(super.y+(super.height/2))));
		
		
		EBullets e0 = new EBullets(1.5,vector.angle(),super.isred,super.x+(super.width/2), super.y+(super.height/2));
		eb.add(e0);
		EBullets e1 = new EBullets(1.5,vector.angle()+2,super.isred,super.x+(super.width/2), super.y+(super.height/2));
		eb.add(e1);
		EBullets e2 = new EBullets(1.5,vector.angle()+4,super.isred,super.x+(super.width/2), super.y+(super.height/2));
		eb.add(e2);
		EBullets e3 = new EBullets(1.5,vector.angle()+6,super.isred,super.x+(super.width/2), super.y+(super.height/2));
		eb.add(e3);
		EBullets e4 = new EBullets(1.5,vector.angle()+8,super.isred,super.x+(super.width/2), super.y+(super.height/2));
		eb.add(e4);
		EBullets e5 = new EBullets(1.5,vector.angle()+10,super.isred,super.x+(super.width/2), super.y+(super.height/2));
		eb.add(e5);
		EBullets e6 = new EBullets(1.5,vector.angle()-2,super.isred,super.x+(super.width/2), super.y+(super.height/2));
		eb.add(e6);
		EBullets e7 = new EBullets(1.5,vector.angle()-4,super.isred,super.x+(super.width/2), super.y+(super.height/2));
		eb.add(e7);
		EBullets e8 = new EBullets(1.5,vector.angle()-6,super.isred,super.x+(super.width/2), super.y+(super.height/2));
		eb.add(e8);
		EBullets e9 = new EBullets(1.5,vector.angle()-8,super.isred,super.x+(super.width/2), super.y+(super.height/2));
		eb.add(e9);
		EBullets e10 = new EBullets(1.5,vector.angle()-10,super.isred,super.x+(super.width/2), super.y+(super.height/2));
		eb.add(e10);
		return eb;
		
	}

}
