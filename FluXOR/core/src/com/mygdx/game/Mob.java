package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;

public abstract class Mob extends Rectangle {
	
	long fireshot;
	 boolean isred;
	 boolean isLeft;
	 Texture sprite;
	 int HP;
	 boolean firable;
	 boolean bloommove;
	 boolean type6move;
	abstract void update();
	abstract Array<EBullets> createbull(float x, float y); 
	double firetime;
}
