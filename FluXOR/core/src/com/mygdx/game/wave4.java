package com.mygdx.game;

import java.util.Iterator;

import com.badlogic.gdx.utils.Array;

public class wave4 {
	
	Array<Mob> moblist;
	int numEnemy = 5;
	boolean isLeft;
	boolean isRed;
	int startingy;
	private static final int SCREEN_WIDTH = 480;
	private static final int mobsize = 100;
	
	public wave4(int y)
	{
		moblist = new Array<Mob>();
		this.startingy = y;
		if(Math.random() > .5)
		{
			isLeft = true;
			for(int i = 1; i <= numEnemy; i++)
			{
				double tt = Math.random()*1000+500;
				isRed = (Math.random() > .5);
				type4 m = new type4(tt,this.isRed,this.isLeft,SCREEN_WIDTH+(mobsize*i),this.startingy);
				moblist.add(m);
			}
		}
		else
		{
			isLeft = false;
			for(int i = 1; i <= numEnemy; i++)
			{
				double tt = Math.random()*1000+500;
				isRed = (Math.random() > .5);
				type4 m = new type4(tt,this.isRed,this.isLeft,0-(mobsize*i),this.startingy);
				moblist.add(m);
			}
		}
		
		
		
	}
	
	public Array<Mob> getlist()
	{
		return moblist;
	}

}