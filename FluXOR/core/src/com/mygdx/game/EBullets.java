package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;

public class EBullets extends Circle {
	
	Texture sprite;
	boolean red;
	private int radi;
	private float type;
	double speed = 1;
	
	public EBullets(int type,boolean isRed,float xpos, float ypos)
	{
		this.type = type;
		this.red = isRed;
		this.radi = 12;
		super.x = xpos-radi;
		super.y = ypos-radi;
		super.radius = radi;
		if(this.red)
		{
			sprite = new Texture(Gdx.files.internal("enemybullred.png"));
		}
		else
		{
			sprite = new Texture(Gdx.files.internal("enemybullblue.png"));
		}
	}
	public EBullets(double speed,float type,boolean isRed,float xpos, float ypos)
	{
		this.speed = speed;
		this.type = type;
		this.red = isRed;
		this.radi = 12;
		super.x = xpos-radi;
		super.y = ypos-radi;
		super.radius = radi;
		if(this.red)
		{
			sprite = new Texture(Gdx.files.internal("enemybullred.png"));
		}
		else
		{
			sprite = new Texture(Gdx.files.internal("enemybullblue.png"));
		}
	}
	
	public EBullets(float type,boolean isRed,float xpos, float ypos)
	{
		this.type = type;
		this.red = isRed;
		this.radi = 12;
		super.x = xpos-radi;
		super.y = ypos-radi;
		super.radius = radi;
		if(this.red)
		{
			sprite = new Texture(Gdx.files.internal("enemybullred.png"));
		}
		else
		{
			sprite = new Texture(Gdx.files.internal("enemybullblue.png"));
		}
	}
	
	public void update()
	{
		Vector2 vector = new Vector2(0,1);
		  vector = (vector.setAngle(type)).nor();
		  super.x += this.speed*180*vector.x*Gdx.graphics.getDeltaTime();
		  super.y += this.speed*180*vector.y*Gdx.graphics.getDeltaTime();
		  //per.y -= 180*Gdx.graphics.getDeltaTime();
	  
	  
	}

}
