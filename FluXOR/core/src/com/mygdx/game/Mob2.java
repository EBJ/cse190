package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

public class Mob2 extends Mob
	{
	int t;
	int bt;
	
	

	public Mob2(double firerate,int type,int btype,boolean red, boolean left,int startingx,int startingy)
	{
		super.firetime = firerate;
		t = type;
		bt = btype;
		super.isred = red;
		super.isLeft=left;
		super.x = startingx;
		super.y =startingy;
		super.HP = 4;
		super.width = 68;
		super.height = 60;
		if(left)
		{
			if(red)
			{
			super.sprite = new Texture(Gdx.files.internal("redmob2.png"));
			}
			else
			{
			super.sprite = new Texture(Gdx.files.internal("bluemob2.png"));	
			}
		}
		else
		{
			if(red)
			{
			super.sprite = new Texture(Gdx.files.internal("redmob2.png"));
			}
			else
			{
			super.sprite = new Texture(Gdx.files.internal("bluemob2.png"));	
			}
		}
	}
	
	
	
	public void update()
	{
		if(t == 1)
		{
		if(isLeft)
		{
			super.x -= 200*Gdx.graphics.getDeltaTime();
			super.y = (float) ((.008)*((super.x - 240)*(super.x - 240)) + 400);
		}
		else
		{
			super.x += 200*Gdx.graphics.getDeltaTime();
			super.y = (float) ((.008)*((super.x - 240)*(super.x - 240)) + 400);
		}
		}
		else if( t == 2)
		{
			super.y -= 200*Gdx.graphics.getDeltaTime();
		}
	}
	
	public Array<EBullets> createbull(float pxpos,float pypos)
	{
		super.fireshot = TimeUtils.millis();
		/*
		Array<EBullets> eb = new Array<EBullets>();
		EBullets e0 = new EBullets(270,super.isred,super.x+(super.width/2), super.y);
		eb.add(e0);
		return eb;
		*/
		if(bt == 1)
		{
		Array<EBullets> eb = new Array<EBullets>();
		EBullets e0 = new EBullets(270,super.isred,super.x+(super.width/2), super.y);
		eb.add(e0);
		EBullets e1 = new EBullets(265,super.isred,super.x+(super.width/2), super.y);
		eb.add(e1);
		EBullets e2 = new EBullets(275,super.isred,super.x+(super.width/2), super.y);
		eb.add(e2);
		return eb;
		}
		else 
		{
		Array<EBullets> eb = new Array<EBullets>();
		EBullets e0 = new EBullets(1.5,270,super.isred,super.x+(super.width/2), super.y);
		eb.add(e0);
		EBullets e1 = new EBullets(1.25,265,super.isred,super.x+(super.width/2), super.y);
		eb.add(e1);
		EBullets e2 = new EBullets(1,275,super.isred,super.x+(super.width/2), super.y);
		eb.add(e2);
		EBullets e3 = new EBullets(.75,265,super.isred,super.x+(super.width/2), super.y);
		eb.add(e3);
		EBullets e4 = new EBullets(.5,275,super.isred,super.x+(super.width/2), super.y);
		eb.add(e4);
		return eb;	
		}
		
	}

}
