package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.Array;

public class Bloom extends Mob
	{

	
	public Bloom(boolean red, boolean left,int startingx,int startingy)
	{
		super.bloommove = false;
		super.firable = false;
		super.isred = red;
		super.isLeft=left;
		super.x = startingx;
		super.y =startingy;
		super.HP = 10;
		super.width = 100;
		super.height = 100;
		if(left)
		{
			if(red)
			{
			super.sprite = new Texture(Gdx.files.internal("bloomred.png"));
			}
			else
			{
			super.sprite = new Texture(Gdx.files.internal("bloomblue.png"));	
			}
		}
		else
		{
			if(red)
			{
			super.sprite = new Texture(Gdx.files.internal("bloomred.png"));
			}
			else
			{
			super.sprite = new Texture(Gdx.files.internal("bloomblue.png"));	
			}
		}
	}
	
	
	
	public void update()
	{
		if(isLeft)
		{
			super.x = 100;
			if(super.y > 500 )
			{
			super.y -= 300*Gdx.graphics.getDeltaTime();
			}
			else
			{
				super.firable = true;
			}
			if(bloommove )
			{
			super.y -= 300*Gdx.graphics.getDeltaTime();
			}
		}
		else
		{
			super.x = 280;
			if(super.y > 500  )
			{
			super.y -= 300*Gdx.graphics.getDeltaTime();
			}
			else
			{
				super.firable = true;
			}
			if(bloommove )
			{
			super.y -= 300*Gdx.graphics.getDeltaTime();
			}
		}
	}
	
	public Array<EBullets> createbull(float pxpos,float pypos)
	{
		/*
		Array<EBullets> eb = new Array<EBullets>();
		EBullets e0 = new EBullets(270,super.isred,super.x+(super.width/2), super.y);
		eb.add(e0);
		return eb;
		*/
		Array<EBullets> eb = new Array<EBullets>();
		EBullets e0 = new EBullets(270,super.isred,super.x+(super.width/2), super.y+(super.height/2));
		eb.add(e0);
		EBullets e1 = new EBullets(306,super.isred,super.x+(super.width/2), super.y+(super.height/2));
		eb.add(e1);
		EBullets e2 = new EBullets(342,super.isred,super.x+(super.width/2), super.y+(super.height/2));
		eb.add(e2);
		EBullets e3 = new EBullets(234,super.isred,super.x+(super.width/2), super.y+(super.height/2));
		eb.add(e3);
		EBullets e4 = new EBullets(198,super.isred,super.x+(super.width/2), super.y+(super.height/2));
		eb.add(e4);
		EBullets e5 = new EBullets(162,super.isred,super.x+(super.width/2), super.y+(super.height/2));
		eb.add(e5);
		EBullets e6 = new EBullets(126,super.isred,super.x+(super.width/2), super.y+(super.height/2));
		eb.add(e6);
		EBullets e7 = new EBullets(90,super.isred,super.x+(super.width/2), super.y+(super.height/2));
		eb.add(e7);
		EBullets e8 = new EBullets(54,super.isred,super.x+(super.width/2), super.y+(super.height/2));
		eb.add(e8);
		EBullets e9 = new EBullets(18,super.isred,super.x+(super.width/2), super.y+(super.height/2));
		eb.add(e9);
		return eb;
		
	}

}
