package com.mygdx.game;

import java.util.Iterator;





import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;


public class FluxXor extends Game implements ApplicationListener {
   private int flux;
   private Texture dropImage;
   private Texture bucketImage;
   private Texture backdrop;
   private Texture stationary;
   private Texture sheild;
   private Texture bombwave;
   private Sound shotsound;
   private Music backMusic;
   private SpriteBatch batch;
   private OrthographicCamera camera;
   private Circle bucket;
   private Circle playercore;
   private Circle absorber;
   private Rectangle back;
   private Rectangle back3;
   private Rectangle back4;
   private Rectangle bombrectangle;
   private Rectangle back2;
   private Array<Rectangle> playerbulls;
   private Array<Mob> enemies;
   private Array<Mob> spenemies;
   private Array<Mob> type6enemies;
   private Array<EBullets> eb;
   private long lastDropTime;
   private long lastenemyspawn;
   private long starttimechange;
   private long startexplosion;
   private long pstartexplosion;
   private long bloomrate;
   private long travelrate;
   private int bloomctr;
   private long type6rate;
   private int type6ctr;
   private boolean bloomon;
   private boolean type6on;
   private boolean blueon;
   private boolean stageon;
   private boolean stagedeath;
   private boolean completed;
   private boolean bombon;
   private static final int FRAME_NUM = 6;
   private static final int explo_frame = 14;
   private Animation changeanimation;
   private Animation changeanimation2;
   private Texture anisheet;
   private Texture explosheet;
   private Texture pexplosheet;
   private Texture imaima;
   private Texture hud;
   private Texture barbar;
   private Texture test;
   private Animation exploanimation;
   private Animation pexploanimation;
   private TextureRegion[] aniframs;
   private TextureRegion[] aniframs2;
   private TextureRegion[] exploframes;
   private TextureRegion[] pexploframes;
   private TextureRegion curr1;
   private TextureRegion curr2;
   private TextureRegion explomob;
   private TextureRegion pexplomob;
   private boolean explotime;
   private boolean pexplotime;
   private float state;
   private float explostate;
   private float pexplostate;
   private Table table;
   private Table t3;
   private Table t4;
   private Stage stage;
   private TextButton button;
   private TextButton boombutton;
   private TextButton bombbutton;
   private TextButton pausebutton;
   private TextButton retrybutton;
   private TextButton quitbutton;
   private TextButton mainbutton;
   private TextButtonStyle textButtonStyle;
   private TextButtonStyle bombStyle;
   private TextButtonStyle pauseStyle;
   private TextButtonStyle boomStyle;
   private BitmapFont font;
   private BitmapFont font2;
   private Skin skin;
   private TextureAtlas buttonAtlas;
   private TextureAtlas pauseAtlas;
   private TextureAtlas boomAtlas;
   private double bombrange = 0;
   private boolean pause = true;
   private Label bombcount;
   private Label absorbcount;
   private Label shipscount;
   private Label distancecount;
   private Label absorbcalc;
   private Label shipscalc;
   private Label distancecalc;
   private Label totalcalc;
   private float explox;
   private float exploy;
   private float pexplox;
   private float pexploy;
   private Image ima;
   private Image huh;
   private Image bar;
   private int absorbctr;
   private int distancectr;
   private int destroyctr;
   public enum State
   {
	   PAUSE,
	   RESUME,
	   END,
	   DEATH
   }
   private State gamestate = State.RESUME;
   
   public FluxXor(int fluxAmount){
	   flux = fluxAmount;
   }
   
   public interface MyGameCallback {
       public void onActivityA(int newflux);
       
   }
   
   private MyGameCallback myGameCallback;
   
   
   public void setMyGameCallback(MyGameCallback callback) {
       myGameCallback = callback;
   }
   
   
   
   @Override
   public void create() {
	   absorbctr = 0;
	   distancectr = 0;
	   destroyctr = 0;
	   travelrate = TimeUtils.millis();
	   stageon = false;
	   stagedeath = false;
      dropImage = new Texture(Gdx.files.internal("doublebullet.png"));
      bucketImage = new Texture(Gdx.files.internal("ikaruga_red.png"));
      backdrop = new Texture(Gdx.files.internal("orange backbackground.png"));
      test = new Texture(Gdx.files.internal("testplayercore.png"));
      stationary = new Texture(Gdx.files.internal("backdrop.png"));
      shotsound = Gdx.audio.newSound(Gdx.files.internal("shot.wav"));
      backMusic = Gdx.audio.newMusic(Gdx.files.internal("start.mp3"));
      sheild = new Texture(Gdx.files.internal("circle.png"));
      bombwave = new Texture(Gdx.files.internal("bombwave.png"));
      imaima = new Texture(Gdx.files.internal("pauseback.jpg"));
      hud = new Texture(Gdx.files.internal("hud.png"));
      barbar = new Texture(Gdx.files.internal("bombbar.png"));
      backMusic.setLooping(true);
      backMusic.play();

      bloomctr = 0;
      bloomon = true;
      type6ctr=0;
      type6on = true;
      // create the camera and the SpriteBatch
      camera = new OrthographicCamera();
      camera.setToOrtho(false, 480, 800);
      bombrectangle = new Rectangle();
      bombrectangle.x = 0;
      bombrectangle.y = -72;
      bombrectangle.height = 72;
      bombrectangle.width = 480;
      batch = new SpriteBatch();
      back = new Rectangle();
      back.x=0;
      back.y=0;
      back.width = 1;
      back.height = 1;
      back3 = new Rectangle();
      back3.x=0;
      back3.y=0;
      back3.width = 1;
      back3.height = 1;
      back4 = new Rectangle();
      back4.x=0;
      back4.y=798;
      back4.width = 1;
      back4.height = 1;
      back2 = new Rectangle();
      back2.x=0;
      back2.y=798;
      back2.width = 1;
      back2.height = 1;
      // create a Rectangle to logically represent the bucket
      bucket = new Circle();
      bombon = false;
      bucket.radius = 40;
      bucket.x = 480 / 2 - bucket.radius / 2; // center the bucket horizontally
      bucket.y = bucket.radius+100; // bottom left corner of the bucket is 20 pixels
      playercore = new Circle(bucket.x,bucket.y,1);
      blueon=true;
      absorber = new Circle(bucket.x,bucket.y,35);
      
      
      
      
      
      
      stage = new Stage();
      Gdx.input.setInputProcessor(stage);
      font = new BitmapFont(Gdx.files.internal("bauhaus24.fnt"));
      font2 = new BitmapFont(Gdx.files.internal("bauhaus34.fnt"));
      skin = new Skin();
      Skin skin2 = new Skin();
      Skin skin3 = new Skin();
      boomAtlas = new TextureAtlas(Gdx.files.internal("uiskin.atlas"));
      buttonAtlas = new TextureAtlas(Gdx.files.internal("gamebutton.atlas"));
      pauseAtlas = new TextureAtlas(Gdx.files.internal("pause.atlas"));
      skin2.addRegions(pauseAtlas);
      skin.addRegions(buttonAtlas);
      skin3.addRegions(boomAtlas);
      textButtonStyle = new TextButtonStyle();
      textButtonStyle.font = font;
      textButtonStyle.up = skin.getDrawable("blue");
      textButtonStyle.down = skin.getDrawable("between");
      textButtonStyle.checked = skin.getDrawable("red");
      pauseStyle = new TextButtonStyle();
      pauseStyle.font = font;
      pauseStyle.up = skin2.getDrawable("pause");
      pauseStyle.down = skin2.getDrawable("play");
      pauseStyle.checked = skin2.getDrawable("play");
      bombStyle = new TextButtonStyle();
      bombStyle.font = font;
      bombStyle.up = skin.getDrawable("bomb2");
      
      bombStyle.down = skin.getDrawable("bomb");
      
      bombStyle.checked = skin.getDrawable("bomb2");
      
      boomStyle = new TextButtonStyle();
      boomStyle.font = font;
      boomStyle.up = skin3.getDrawable("default-scroll");
      boomStyle.down = skin3.getDrawable("default-round-large");
      boomStyle.checked = skin3.getDrawable("default-scroll");
      pausebutton =new TextButton(" ", pauseStyle);
      button = new TextButton("POLARITY", textButtonStyle);
      //button.setColor(button.getColor().r, button.getColor().g, button.getColor().b, 0.1f);
      bombbutton = new TextButton("BOMB",bombStyle);
      boombutton = new TextButton("RESUME",boomStyle);
      retrybutton = new TextButton("RETRY",boomStyle);
      quitbutton = new TextButton("QUIT",boomStyle);
      mainbutton = new TextButton("QUIT",boomStyle);
      Label.LabelStyle l = new Label.LabelStyle(font,Color.WHITE);
      t3 = new Table();
      t3.setX(240);
      t3.setY(400);
      t3.add(boombutton).width(280).row();
      t3.row().height(50);
      t3.row();
      t3.add(mainbutton).width(280).row();
      t4 = new Table();
      t4.setX(240);
      t4.setY(400);
      t4.add(retrybutton).width(280).row();
      t4.row().height(50);
      t4.row();
      t4.add(quitbutton).width(280).row();
      Table t2 = new Table();
      t2.row().height(50);
      t2.setX(450);
      t2.setY(750);
      t2.add(pausebutton).width(50);
      table = new Table();
      table.setX(300);
      table.setY(50);
      table.row().height(100);
      bombcount = new Label(""+bombrange,l);
      absorbcount = new Label(""+absorbctr,l);
      shipscount = new Label(""+destroyctr,l);
      distancecount = new Label(""+distancectr,l);
      bombcount.setWrap(true);
      
      table.add(bombcount).width(150);
      table.add(bombbutton).width(100);
      table.add(button).width(100);
      stage.addActor(t2);
      stage.addActor(table);
      ima = new Image(imaima);
      huh = new Image(hud);
      huh.setX(0);
      huh.setY(0);
      bar = new Image(barbar);
      stage.addActor(huh);
      bar.setX(17);
      bar.setY(8);
      stage.addActor(bar);
      distancecount.setX(60);
      distancecount.setY(60);
      absorbcount.setX(140);
      absorbcount.setY(60);
      shipscount.setX(225);
      shipscount.setY(60);
      stage.addActor(distancecount);
      stage.addActor(shipscount);
      stage.addActor(absorbcount);
      stage.getViewport().setCamera(camera);
      
      
      
      
      explosheet = new Texture(Gdx.files.internal("enemyexplosion.png"));
      pexplosheet = new Texture(Gdx.files.internal("playerexplosion.png"));
      anisheet = new Texture(Gdx.files.internal("shipchange.png"));
      TextureRegion[][] tmp2 = TextureRegion.split(explosheet, explosheet.getWidth()/explo_frame, 60);
      TextureRegion[][] tmp3 = TextureRegion.split(pexplosheet, pexplosheet.getWidth()/explo_frame, 110);
      TextureRegion[][] tmp = TextureRegion.split(anisheet, anisheet.getWidth()/FRAME_NUM,100); 
      exploframes = new TextureRegion[explo_frame];
      pexploframes = new TextureRegion[explo_frame];
      aniframs = new TextureRegion[FRAME_NUM];
      aniframs2 = new TextureRegion[FRAME_NUM];
      int index = 0;
      for(int i = 0; i < FRAME_NUM; i++)
      {
      aniframs[index++] = tmp[0][i];
      }
      int index2=0;
      for(int i = FRAME_NUM-1; i >=0; i--)
      {
      aniframs2[index2++] = tmp[0][i];
      }
      int index3=0;
      for(int i = 0;i<explo_frame;i++)
      {
    	  exploframes[index3++]=tmp2[0][i];
      }
      int index4=0;
      for(int i = 0;i<explo_frame;i++)
      {
    	  pexploframes[index4++]=tmp3[0][i];
      }
      pexploanimation = new Animation(0.055f,pexploframes);
      exploanimation = new Animation(0.024f,exploframes);
      changeanimation = new Animation(0.055f,aniframs);
      changeanimation2 = new Animation(0.055f,aniframs2);
      state = 0f;
      explostate = 0f;
      pexplostate = 0f;
      // create the raindrops array and spawn the first raindrop
      playerbulls = new Array<Rectangle>();
      spawnbullet();
      
      enemies = new Array<Mob>();
      spenemies = new Array<Mob>();
      type6enemies = new Array<Mob>();
      eb = new Array<EBullets>();
      
      ChangeListener cl = new ChangeListener()
      {
    	  public void changed(ChangeEvent event, Actor actor) {
    		 
    		  if(blueon)
    	  	  {
    	  		  sheild = new Texture(Gdx.files.internal("red-circle.png"));
    	  		  dropImage = new Texture(Gdx.files.internal("doubleredbull.png"));
    	  		  completed = false;
    	  		  starttimechange = TimeUtils.millis();
    	  		  state = 0f;
    			  blueon=false;
    	  	  }
    		  else
    		  {
    			  sheild = new Texture(Gdx.files.internal("circle.png"));
    	  		  dropImage = new Texture(Gdx.files.internal("doublebullet.png"));
    	  		  completed = false;
    	  		  starttimechange = TimeUtils.millis();
    	  		  state = 0f;
    			  blueon=true;
    		  }
    		  
    		  
    		  
    		  
    	  }
      };
      button.addListener(cl);
      ChangeListener bl = new ChangeListener()
      {
    	  public void changed(ChangeEvent event, Actor actor) {
    		 
    		  if(bombrange >= 100)
    		  {
    			  bombon = true;
    			  bombrange -= 100;
    			  bombcount.setText(""+bombrange);
    		  }
    		  
    		  
    		  
    		  
    	  }
      };
      bombbutton.addListener(bl);
      
      
      ChangeListener pl = new ChangeListener()
      {
    	  public void changed(ChangeEvent event, Actor actor) {
    		 
    			  pause();
    			  pause = false;
    		  
    		  
    		  
    		  
    	  }
      };
      pausebutton.addListener(pl);
      
      
      ChangeListener ppl = new ChangeListener()
      {
    	  public void changed(ChangeEvent event, Actor actor) { 
    		  
    			  resume();
    			  pause=true;	  
    		  
    	  }
      };
      boombutton.addListener(ppl);
      retrybutton.addListener(ppl);
      ChangeListener qql = new ChangeListener()
      {
    	  public void changed(ChangeEvent event, Actor actor) { 
    		  
    			  quit();	      		  
    	  }
      };
      quitbutton.addListener(qql);
      mainbutton.addListener(qql);
      
   }//end create

   private void spawnbullet() {
      Rectangle bull = new Rectangle();
      bull.x = bucket.x-(dropImage.getWidth()/2);
      bull.y = bucket.y;
      bull.width = dropImage.getWidth();
      bull.height = dropImage.getHeight();
      playerbulls.add(bull);
      //shotsound.play();
      lastDropTime = TimeUtils.nanoTime();
   }
   
   private void spawnenemy() {
	   double prob = Math.random();
	   if(prob < .24)
	   {
      	 Wave1 w = new Wave1(600);
      	 enemies.addAll(w.getlist());
	   }
	   else if(prob < .48 && prob >= .24)
	   {
		   Wave2 w = new Wave2(600);
	      enemies.addAll(w.getlist());  
	   }
	   else if(prob < .72 && prob >= .48 )
	   {
		   wave4 w = new wave4(600);
		   enemies.addAll(w.getlist());
		   
	   }
	   else if(prob < .9 && prob >= .72 )
	   {
		   rain3 w = new rain3(600);
		   enemies.addAll(w.getlist()); 
	   }
	   else if(prob <= 1 && prob >= .9 )
	   {
		   type6wave w = new type6wave(900,playercore.x,playercore.y);
		   type6enemies.addAll(w.getlist());
	   }
	   if(Math.random() > .9)
	   {
		   BloomWave bw = new BloomWave(900);
		   spenemies.addAll(bw.getlist());
	   }
         lastenemyspawn = TimeUtils.millis();
   }

   @Override
   public void render() {
	  switch(gamestate)
	  {
	  case RESUME:
	  
      // clear the screen with a dark blue color. The
      // arguments to glClearColor are the red, green
      // blue and alpha component in the range [0,1]
      // of the color to be used to clear the screen.
      Gdx.gl.glClearColor(0, 0, (float) 0.0, 0);
      Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
      
      // tell the camera to update its matrices.
      camera.update();

      // tell the SpriteBatch to render in the
      // coordinate system specified by the camera.
      batch.setProjectionMatrix(camera.combined);
      bar.setWidth((float) (bombrange*2.5));
      // begin a new batch and draw the bucket and
      // all drops
      explostate += Gdx.graphics.getDeltaTime();
      pexplostate += Gdx.graphics.getDeltaTime();
      pexplomob = pexploanimation.getKeyFrame(pexplostate,false);
      explomob = exploanimation.getKeyFrame(explostate,false);
      state += Gdx.graphics.getDeltaTime();        
      curr1 = changeanimation.getKeyFrame(state, false);
      curr2 = changeanimation2.getKeyFrame(state, false);
      if(stageon)
      {
    	  ima.remove();
    	  t3.remove();
    	  stageon = false;
      }
      if(stagedeath)
      {
    	  ima.remove();
    	  t4.remove();
    	  absorbcalc.remove();
		  shipscalc.remove();
		  distancecalc.remove();
		  totalcalc.remove();
    	  stagedeath = false;
      }
      Iterator<Mob> bullm = enemies.iterator();
      while(bullm.hasNext())
      {
    	  Mob mobby = bullm.next();
    	  if(TimeUtils.millis() - mobby.fireshot > mobby.firetime)
          {
    		  Array<EBullets> ebull = mobby.createbull(playercore.x,playercore.y);
        	  eb.addAll(ebull);
          }
    	  
    	  
    	  
      }
      
      Iterator<Mob> spbm = spenemies.iterator();
      while(spbm.hasNext())
      {
    	  Mob moby = spbm.next();
    	  if( moby.firable && bloomon)
    	  {
    	 bloomctr++;
    	  Array<EBullets> ebull = moby.createbull(playercore.x,playercore.y);
    	  eb.addAll(ebull);
    	  bloomrate = TimeUtils.millis();
   
    	  }
    	  
      }
      bloomon = false;
      
      
      
      Iterator<Mob> t6mb = type6enemies.iterator();
      while(t6mb.hasNext())
      {
    	  Mob moby = t6mb.next();
    	  if( moby.firable && type6on)
    	  {
    	 type6ctr++;
    	  Array<EBullets> ebull = moby.createbull(playercore.x,playercore.y);
    	  eb.addAll(ebull);
    	  type6rate = TimeUtils.millis();
   
    	  }
    	  
      }
      type6on = false;
      
      batch.begin();
      //backsprite.draw(batch);
      batch.draw(backdrop,back.x,back.y);
      batch.draw(backdrop,back2.x,back2.y);
      batch.draw(stationary, back3.x,back3.y);
      batch.draw(stationary, back4.x,back4.y);
      for(Rectangle raindrop: playerbulls) {
         batch.draw(dropImage, raindrop.x, raindrop.y);
      }
      //batch.draw(curr, bucket.x,bucket.y);
      
      
      if(blueon)
      {
    	  batch.draw(curr1, bucket.x-28,bucket.y-50);  
      }
      else
      {
    	  
    	  batch.draw(curr2, bucket.x-28,bucket.y-50);  
    	  
      }
      batch.draw(sheild,absorber.x-(sheild.getWidth()/2),absorber.y-(sheild.getHeight()/2) );
      
     
      for(int i = 0; i < enemies.size; i++)
      {
    	  batch.draw(enemies.get(i).sprite,enemies.get(i).x,enemies.get(i).y);
      }
      for(int i = 0; i < spenemies.size; i++)
      {
    	  batch.draw(spenemies.get(i).sprite,spenemies.get(i).x,spenemies.get(i).y);
      }
      for(int i = 0; i < type6enemies.size; i++)
      {
    	  batch.draw(type6enemies.get(i).sprite,type6enemies.get(i).x,type6enemies.get(i).y);
      }
      
      for(int i = 0; i < eb.size; i++)
      {
    	  batch.draw(eb.get(i).sprite, eb.get(i).x-eb.get(i).radius,eb.get(i).y-eb.get(i).radius);
      }
      
      if(explotime)
      {
    	  batch.draw(explomob,explox,exploy);
      }
      
      if(pexplotime)
      {
    	  batch.draw(pexplomob,pexplox,pexploy);
      }
      if(bombon)
      {
      batch.draw(bombwave,bombrectangle.x,bombrectangle.y);
      }
      batch.draw(test,playercore.x-1,playercore.y-1);
      batch.end();
      stage.draw();
      // process user input
      if(Gdx.input.isTouched()) {
         Vector3 touchPos = new Vector3();
         touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
         camera.unproject(touchPos);
         
         float ratioX = Math.abs(touchPos.x - bucket.x);
         float ratioY = Math.abs(touchPos.y - bucket.y);
         float Unit = (float) Math.sqrt(((ratioX*ratioX)+(ratioY*ratioY)));
         float ratio = Math.abs((ratioY/Unit));
         float ratioInv = Math.abs((ratioX/Unit));
         
         
         if(  true /*button movement*/)
         {
         
         if(touchPos.x - bucket.x >= 300* Gdx.graphics.getDeltaTime())
         {
        	 
             bucket.x += ratioInv*300 * Gdx.graphics.getDeltaTime();	 
        	 
         }
         if(touchPos.x - bucket.x <= 300* Gdx.graphics.getDeltaTime())
         {
        	 
             bucket.x -= ratioInv*300 * Gdx.graphics.getDeltaTime();	 
        	 
         }
         if(touchPos.y+75 - bucket.y >= 355* Gdx.graphics.getDeltaTime())
         {
        	 
        	 
        	 bucket.y += ratio*355 * Gdx.graphics.getDeltaTime();	 
        	 
        	 
         }
         if(touchPos.y+75 - bucket.y <= 355* Gdx.graphics.getDeltaTime())
         {
        	 
        	 bucket.y -= ratio*355 * Gdx.graphics.getDeltaTime();	 
        	 
         }
         
         }
         
         
        // bucket.x = touchPos.x - 58 / 2;
        // bucket.y = touchPos.y-45;
         absorber.x = bucket.x;
         absorber.y = bucket.y;
         playercore.x = bucket.x;
         playercore.y = bucket.y;
      }
      
      

      
      // make sure the bucket stays within the screen bounds
      if(bombrectangle.y < 800 && bombon)
      {
    	  bombrectangle.y += 10;
      }
      if(bombrectangle.y > 800 && bombon)
      {
    	  bombon = false;
    	  bombrectangle.y = -72;
      }
      if(bucket.x < bucket.radius) bucket.x = bucket.radius;
      if(bucket.x > 480 - bucket.radius) bucket.x = 480 - bucket.radius;
      if(bucket.y < bucket.radius+100) bucket.y = bucket.radius+100;
      if(bucket.y > 800 - bucket.radius) bucket.y = 800 - bucket.radius;
      if(back.y <= 798)
      {
    	  back.y+= 8;
      }
      else
      {
    	  back.y=-798;
      }
      if(back2.y <= 798)
      {
    	  back2.y+= 8;
      }
      else
      {
    	  back2.y=-798;
      }
      
      if(back3.y >= -798)
      {
    	  back3.y-= 4;
      }
      else
      {
    	  back3.y=798;
      }
      if(back4.y >= -798)
      {
    	  back4.y-= 4;
      }
      else
      {
    	  back4.y=798;
      }
      
      
      
      if(TimeUtils.millis() - starttimechange > 333)
      {
    	  completed = true;
      }
      
      if(TimeUtils.millis() - startexplosion > 333)
      {
    	  explotime = false;
      }
      if(TimeUtils.millis() - bloomrate > 500)
      {
    	  bloomon = true;
      }
      if(TimeUtils.millis() - type6rate > 500)
      {
    	  type6on = true;
      }
      if(TimeUtils.millis() - travelrate > 60)
      {
    	  distancectr+=1;
    	  distancecount.setText(""+distancectr);
    	  travelrate = TimeUtils.millis();
      }
      if(bloomctr > (6*spenemies.size)+1)
      {
    	  for(int i = 0; i < spenemies.size;i++ )
    	  {
    		 spenemies.get(i).bloommove = true; 
    	  }
    	  bloomctr =0;
      }
      if(type6ctr > (6*type6enemies.size)+1)
      {
    	  for(int i = 0; i < type6enemies.size;i++ )
    	  {
    		 type6enemies.get(i).type6move = true; 
    	  }
    	  type6ctr =0;
      }
      if(TimeUtils.millis() - pstartexplosion > 666)
      {
    	  if(pexplotime = true)
    	  {
    		  
    	  }
    	  pexplotime = false;
    	  
      }
      
      // check if we need to create a new raindrop
      if(TimeUtils.nanoTime() - lastDropTime > 100000000 && completed) spawnbullet();
      
      if(TimeUtils.millis() - lastenemyspawn > 4000) spawnenemy();
      
      // move the raindrops, remove any that are beneath the bottom edge of
      // the screen or that hit the bucket. In the later case we play back
      // a sound effect as well.
      Iterator<Rectangle> iter = playerbulls.iterator();
      while(iter.hasNext()) {
         Rectangle bull = iter.next();
         bull.y += 2000 * Gdx.graphics.getDeltaTime();
         if(bull.y > 800) iter.remove();
         
      }
      Iterator<Mob> miter = enemies.iterator();
      while(miter.hasNext())
      {
    	  Mob c = miter.next();
    	  c.update();
    	  if(c.x > 1005|| c.x < -525)
    	  {
    		  miter.remove();
    		  continue;
    	  }
    	  
    	  
    	  
    	  
    	  if(Intersector.overlaps(bombrectangle, c))
    	  {
    		     exploy = c.y;
	             explox = c.x;
	             explostate = 0f;
	             startexplosion = TimeUtils.millis();
	             explotime = true;
	             miter.remove();
	             destroyctr+=1;
	             shipscount.setText(""+destroyctr);
	             continue;
    	  }
    	  
    	  if(Intersector.overlaps(playercore, c))
    	  {
    		     exploy = c.y;
	             explox = c.x;
	             explostate = 0f;
	             startexplosion = TimeUtils.millis();
	             explotime = true;
	             miter.remove();
	             pexploy = playercore.y-55;
	             pexplox = playercore.x-55;
	             pexplostate = 0f;
	             pstartexplosion = TimeUtils.millis();
	             pexplotime = true;
	             die();
	             continue;
    	  }
    	  Iterator<Rectangle> biter = playerbulls.iterator();
    	  while(biter.hasNext()) {
    	         Rectangle bull = biter.next();
    	         if(Intersector.overlaps(c, bull))
    	         {
    	        	 
    	        	 
    	        	 biter.remove();
    	        	 if(blueon && c.isred)
    	        	 {
    	        	 c.HP = c.HP -2;
    	        	 }
    	        	 else if(!blueon && !c.isred)
    	        	 {
    	        	 c.HP = c.HP -2;
    	        	 }
    	        	 else
    	        	 {
    	             c.HP = c.HP -1; 
    	        	 }
    	        	 if(c.HP <=0)
    	        	 {
    	             exploy = c.y;
    	             explox = c.x;
    	             explostate = 0f;
    	             startexplosion = TimeUtils.millis();
    	             explotime = true;
    	        	 miter.remove();
    	        	 destroyctr+=1;
    	        	 shipscount.setText(""+destroyctr);
    	        	 continue;
    	        	 }
    	         }
    	         
    	      }
    	  
      }
      
      
      
      Iterator<Mob> bloommiter = spenemies.iterator();
      while(bloommiter.hasNext())
      {
    	  Mob c = bloommiter.next();
    	  c.update();
    	  
    	  if(c.y < -150 )
    	  {
    		  bloommiter.remove();
    		  continue;
    	  }
    	  
    	  
    	  
    	  if(Intersector.overlaps(bombrectangle, c))
    	  {
	             bloommiter.remove();
	             destroyctr+=1;
	             shipscount.setText(""+destroyctr);
	             continue;
    	  }
    	  
    	  
    	  if(Intersector.overlaps(playercore, c))
    	  {
    		     
	             bloommiter.remove();
	             pexploy = playercore.y-55;
	             pexplox = playercore.x-55;
	             pexplostate = 0f;
	             pstartexplosion = TimeUtils.millis();
	             pexplotime = true;	
	             die();
	             continue;
    	  }
    	  
    	  
    	  Iterator<Rectangle> bloombiter = playerbulls.iterator();
    	  while(bloombiter.hasNext()) {
    	         Rectangle bull = bloombiter.next();
    	         if(Intersector.overlaps(c, bull))
    	         {
    	        	 
    	        	 
    	        	 bloombiter.remove();
    	        	 if(blueon && c.isred)
    	        	 {
    	        	 c.HP = c.HP -2;
    	        	 }
    	        	 else if(!blueon && !c.isred)
    	        	 {
    	        	 c.HP = c.HP -2;
    	        	 }
    	        	 else
    	        	 {
    	             c.HP = c.HP -1; 
    	        	 }
    	        	 if(c.HP <=0)
    	        	 {
    	        		 pexploy = c.y;
    		             pexplox = c.x;
    		             pexplostate = 0f;
    		             pstartexplosion = TimeUtils.millis();
    		             pexplotime = true;
    		             bloommiter.remove();
    		             destroyctr+=1;
    		             shipscount.setText(""+destroyctr);
    		             continue;
    	        	 
    	        	 }
    	         }
    	         
    	      }
      }
      
      
      
      
      
      Iterator<Mob> type6miter = type6enemies.iterator();
      while(type6miter.hasNext())
      {
    	  Mob c = type6miter.next();
    	  c.update();
    	  
    	  if(c.y > 1000 )
    	  {
    		  type6miter.remove();
    		  continue;
    	  }
    	  
    	  
    	  
    	  if(Intersector.overlaps(bombrectangle, c))
    	  {
	             type6miter.remove();
	             destroyctr+=1;
	             shipscount.setText(""+destroyctr);
	             continue;
    	  }
    	  
    	  
    	  if(Intersector.overlaps(playercore, c))
    	  {
    		     
	             type6miter.remove();
	             pexploy = playercore.y-55;
	             pexplox = playercore.x-55;
	             pexplostate = 0f;
	             pstartexplosion = TimeUtils.millis();
	             pexplotime = true;	
	             continue;
    	  }
    	  
    	  
    	  Iterator<Rectangle> type6biter = playerbulls.iterator();
    	  while(type6biter.hasNext()) {
    	         Rectangle bull = type6biter.next();
    	         if(Intersector.overlaps(c, bull))
    	         {
    	        	 
    	        	 
    	        	 type6biter.remove();
    	        	 if(blueon && c.isred)
    	        	 {
    	        	 c.HP = c.HP -2;
    	        	 }
    	        	 else if(!blueon && !c.isred)
    	        	 {
    	        	 c.HP = c.HP -2;
    	        	 }
    	        	 else
    	        	 {
    	             c.HP = c.HP -1; 
    	        	 }
    	        	 if(c.HP <=0)
    	        	 {
    	        		 pexploy = c.y;
    		             pexplox = c.x;
    		             pexplostate = 0f;
    		             pstartexplosion = TimeUtils.millis();
    		             pexplotime = true;
    		             type6miter.remove();
    		             destroyctr+=1;
    		             shipscount.setText(""+destroyctr);
    		             continue;
    	        	 
    	        	 }
    	         }
    	         
    	      }
      }
      
      
      Iterator<EBullets> eater = eb.iterator();
	  while(eater.hasNext())
	  {
		  EBullets ebu = eater.next();
		  ebu.update();
		  
		  if(ebu.y < 0-ebu.radius || ebu.y > 900)
		  {
			  eater.remove();
			  continue;
		  }
		  if(!ebu.red && blueon)
		  {
			  if(Intersector.overlaps(ebu, absorber))
			  {
				  eater.remove();
				  if(bombrange < 100)
				  {
				  bombrange++;
				  }
				  bombcount.setText(""+bombrange);
				  absorbctr+=1;
				  absorbcount.setText(""+absorbctr);
				  continue;
			  }
		  }
		  if(ebu.red && !blueon)
		  {
			  if(Intersector.overlaps(ebu, absorber))
			  {
				  eater.remove();
				  if(bombrange < 100)
				  {
				  bombrange++;
				  }
				  bombcount.setText(""+bombrange);
				  absorbctr+=1;
				  absorbcount.setText(""+absorbctr);
				  continue;
			  }
		  }
		  if(Intersector.overlaps( ebu,bombrectangle))
    	  {    		     
	             eater.remove();
	             continue;
    	  }
		  if(Intersector.overlaps( playercore,ebu))
    	  {    	
			     eater.remove();
			     pexploy = playercore.y-55;
	             pexplox = playercore.x-55;
	             pexplostate = 0f;
	             pstartexplosion = TimeUtils.millis();
	             pexplotime = true;
	             while(eater.hasNext())
	      	   {
	      		   EBullets eb = eater.next();
	      		   eater.remove();
	      		   
	      	   }
	             die();
    	  }
		  
		  
	  }
	  break;
	  
	  case PAUSE:
		if(stageon)
		{
		stage.addActor(ima);
		stage.addActor(t3);
		}
		stage.draw();  
		  
		  break;
	  case DEATH:
		  if(stagedeath)
			{
			stage.addActor(ima);
			stage.addActor(t4);
				    
		    stage.addActor(absorbcalc);
		    stage.addActor(shipscalc);
		    stage.addActor(distancecalc);
		    stage.addActor(totalcalc);
			}
			stage.draw();
		  break;
	
	  default:
		  break;
	  }
   }

   @Override
   public void dispose() {
      // dispose of all the native resources
	  font.dispose();
	  skin.dispose();
	  explosheet.dispose();
	  anisheet.dispose();
      dropImage.dispose();
      bucketImage.dispose();
      shotsound.dispose();
      backMusic.dispose();
      backdrop.dispose();
      sheild.dispose();
      anisheet.dispose();     
      batch.dispose();
      stage.dispose();
      bombwave.dispose();
      backdrop.dispose();
   }

   @Override
   public void resize(int width, int height) {
   }

   @Override
   public void pause() {
	   
	   stageon = true;
	   this.gamestate = State.PAUSE;
	   this.backMusic.pause();
	   
   }

   public void die()
   {
	   stagedeath = true;
	   this.gamestate = State.DEATH;
	   this.backMusic.pause();
	   bombon = true;	 
	   bombcount.setText(""+bombrange);
	   bucket.x = 480 / 2 - bucket.radius / 2; // center the bucket horizontally
	   bucket.y = bucket.radius+100; // bottom left corner of the bucket is 20 pixels
	   absorber.x = bucket.x;
       absorber.y = bucket.y;
       playercore.x = bucket.x;
       playercore.y = bucket.y;
       Label.LabelStyle l2 = new Label.LabelStyle(font2,Color.WHITE);
	    absorbcalc = new Label("Absorbed Bullets: "+absorbctr,l2);
	    shipscalc = new Label("Destroyed Ships: "+destroyctr,l2);
	    distancecalc = new Label("Distance Traveled: "+distancectr+ " /10",l2);
	    totalcalc = new Label("Total Flux: "+(absorbctr+destroyctr+(distancectr/10)),l2);
	    distancecalc.setX(75);
	    distancecalc.setY(300);
	    absorbcalc.setX(75);
	    absorbcalc.setY(225);
	    shipscalc.setX(75);
	    shipscalc.setY(150);
	    totalcalc.setX(75);
	    totalcalc.setY(75);	
       
   }
   
   
   
   @Override
   public void resume() {

	   lastDropTime = TimeUtils.nanoTime();
	   lastenemyspawn= TimeUtils.millis();
	   starttimechange= TimeUtils.millis();
	   startexplosion= TimeUtils.millis();
	   pstartexplosion= TimeUtils.millis();
	   bloomrate= TimeUtils.millis();
	   type6rate= TimeUtils.millis();
	   travelrate = TimeUtils.millis();
	   this.gamestate = State.RESUME;
	   this.backMusic.play();
   }
   public void quit()
   {
	   flux+= destroyctr;
	   flux+= absorbctr;
	   flux+= (distancectr/10);
	   if (myGameCallback != null) {
		   myGameCallback.onActivityA(flux);
	   }
   }
}