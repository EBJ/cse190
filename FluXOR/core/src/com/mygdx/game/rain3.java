package com.mygdx.game;

import java.util.Iterator;

import com.badlogic.gdx.utils.Array;

public class rain3 {
	
	Array<Mob> moblist;
	int numEnemy = 5;
	boolean isLeft;
	boolean isRed;
	int startingy;
	private static final int SCREEN_WIDTH = 480;
	private static final int mobsize = 70;
	
	public rain3(int y)
	{
		moblist = new Array<Mob>();
		if(Math.random() > .5)
		{
			isLeft = true;
			if(Math.random() > .5)
			{
				isRed = true;
			}
			else
			{
				isRed=false;
			}
			double t5 = Math.random()*1000+500;
			Mob2 m1 = new Mob2(t5,2,2,isRed,this.isLeft,400,810);
			moblist.add(m1);
			double t4 = Math.random()*1000+500;
			Mob2 m2 = new Mob2(t4,2,2,!isRed,this.isLeft,320,870);
			moblist.add(m2);
			double t3 = Math.random()*1000+500;
			Mob2 m3 = new Mob2(t3,2,2,isRed,this.isLeft,240,930);
			moblist.add(m3);
			double t2 = Math.random()*1000+500;
			Mob2 m4 = new Mob2(t2,2,2,!isRed,this.isLeft,160,1000);
			moblist.add(m4);
			double t1 = Math.random()*1000+500;
			Mob2 m5 = new Mob2(t1,2,2,isRed,this.isLeft,80,1070);
			moblist.add(m5);

		}
		else
		{
			isLeft = false;
			if(Math.random() > .5)
			{
				isRed = true;
			}
			else
			{
				isRed = false;
			}
			double t5 = Math.random()*1000+500;
			Mob2 m1 = new Mob2(t5,2,2,isRed,this.isLeft,80,810);
			moblist.add(m1);
			double t4 = Math.random()*1000+500;
			Mob2 m2 = new Mob2(t4,2,2,!isRed,this.isLeft,160,870);
			moblist.add(m2);
			double t3 = Math.random()*1000+500;
			Mob2 m3 = new Mob2(t3,2,2,isRed,this.isLeft,240,930);
			moblist.add(m3);
			double t2 = Math.random()*1000+500;
			Mob2 m4 = new Mob2(t2,2,2,!isRed,this.isLeft,320,1000);
			moblist.add(m4);
			double t1 = Math.random()*1000+500;
			Mob2 m5 = new Mob2(t1,2,2,isRed,this.isLeft,400,1070);
			moblist.add(m5);
		}
		
		
		
	}
	
	public Array<Mob> getlist()
	{
		return moblist;
	}

}