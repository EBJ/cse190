package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

public class type4 extends Mob
	{
	

	public type4(double firetime,boolean red, boolean left,int startingx,int startingy)
	{
		super.firetime = firetime;
		super.isred = red;
		super.isLeft=left;
		super.x = startingx;
		super.y =startingy;
		super.HP = 5;
		super.width = 100;
		super.height = 80;
		if(left)
		{
			if(red)
			{
			super.sprite = new Texture(Gdx.files.internal("type4red.png"));
			}
			else
			{
			super.sprite = new Texture(Gdx.files.internal("type4blue.png"));	
			}
		}
		else
		{
			if(red)
			{
			super.sprite = new Texture(Gdx.files.internal("type4red.png"));
			}
			else
			{
			super.sprite = new Texture(Gdx.files.internal("type4blue.png"));	
			}
		}
	}
	
	
	
	public void update()
	{
		if(isLeft)
		{
			super.x -= 200*Gdx.graphics.getDeltaTime();
			super.y = (float) ((.008)*((super.x - 240)*(super.x - 240)) + 400);
		}
		else
		{
			super.x += 200*Gdx.graphics.getDeltaTime();
			super.y = (float) ((.008)*((super.x - 240)*(super.x - 240)) + 400);
		}
	}
	
	public Array<EBullets> createbull(float pxpos,float pypos)
	{
		super.fireshot = TimeUtils.millis();
		/*
		Array<EBullets> eb = new Array<EBullets>();
		EBullets e0 = new EBullets(270,super.isred,super.x+(super.width/2), super.y);
		eb.add(e0);
		return eb;
		*/
		Array<EBullets> eb = new Array<EBullets>();
		EBullets e0 = new EBullets(270,super.isred,super.x+(super.width/2), super.y);
		eb.add(e0);
		EBullets e1 = new EBullets(266,super.isred,super.x+(super.width/2), super.y);
		eb.add(e1);
		EBullets e2 = new EBullets(274,super.isred,super.x+(super.width/2), super.y);
		eb.add(e2);
		EBullets e3 = new EBullets(262,super.isred,super.x+(super.width/2), super.y);
		eb.add(e3);
		EBullets e4 = new EBullets(278,super.isred,super.x+(super.width/2), super.y);
		eb.add(e4);
		return eb;
		
	}

}
