package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

public class Mob1 extends Mob
	{
	

	public Mob1(double firerate,boolean red, boolean left,int startingx,int startingy)
	{
		super.firetime = firerate;
		super.isred = red;
		super.isLeft=left;
		super.x = startingx;
		super.y =startingy;
		super.HP = 1;
		super.width = 43;
		super.height = 49;
		if(left)
		{
			if(red)
			{
			super.sprite = new Texture(Gdx.files.internal("red_left.png"));
			}
			else
			{
			super.sprite = new Texture(Gdx.files.internal("blue_left.png"));	
			}
		}
		else
		{
			if(red)
			{
			super.sprite = new Texture(Gdx.files.internal("red_right.png"));
			}
			else
			{
			super.sprite = new Texture(Gdx.files.internal("blue_right.png"));	
			}
		}
	}
	
	
	
	public void update()
	{
		if(isLeft)
		{
			super.x -= 200*Gdx.graphics.getDeltaTime();
			//super.y = (float) ((.008)*((super.x - 240)*(super.x - 240)) + 400);
		}
		else
		{
			super.x += 200*Gdx.graphics.getDeltaTime();
		}
	}
	
	public Array<EBullets> createbull(float pxpos,float pypos)
	{
		super.fireshot = TimeUtils.millis();
		Array<EBullets> eb = new Array<EBullets>();
		EBullets e0 = new EBullets(270,super.isred,super.x+(super.width/2), super.y);
		eb.add(e0);
		return eb;
		/*
		Array<EBullets> eb = new Array<EBullets>();
		EBullets e0 = new EBullets(270,super.isred,super.x+(super.width/2), super.y);
		eb.add(e0);
		EBullets e1 = new EBullets(265,super.isred,super.x+(super.width/2), super.y);
		eb.add(e1);
		EBullets e2 = new EBullets(275,super.isred,super.x+(super.width/2), super.y);
		eb.add(e2);
		return eb;
		*/
	}

}
