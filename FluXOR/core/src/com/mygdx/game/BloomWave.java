package com.mygdx.game;

import java.util.Iterator;

import com.badlogic.gdx.utils.Array;

public class BloomWave {
	
	Array<Mob> moblist;
	int numEnemy = 2;
	boolean isLeft;
	boolean isRed;
	int startingy;
	
	public BloomWave(int y)
	{
		moblist = new Array<Mob>();
		this.startingy = y;

	    isLeft = true;


				isRed = (Math.random() > .5);
				Bloom m1 = new Bloom(this.isRed,true,100,this.startingy);
				moblist.add(m1);


	  isLeft = false;

				
				Bloom m2 = new Bloom(!this.isRed,false,280,this.startingy);
				moblist.add(m2);

		
		
		
	}
	
	public Array<Mob> getlist()
	{
		return moblist;
	}

}