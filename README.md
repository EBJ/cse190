# README #

Hello! This is the repository for a mobile application created by Jonathan Lu, Brian Yi, and Edward Lim

### Mobile Game Application "FluXOR ###

* Arcade Shooter Style game
* Version 1.0

### How do I get set up? ###

* Import Files into Android Studio or Eclipse and run.

### Who do I talk to? ###

* Owned by Jonathan Lu, Brian Yi, And Edward Lim
* Created at UCSD
* In class CSE190 under Professor Greg Hoover and TA Nick Kinkade